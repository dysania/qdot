let assert = require("assert");

// support back to node 6.0.0
// strict mode was added in node 9.9.0
// https://nodejs.org/docs/latest-v12.x/api/assert.html#assert_strict_assertion_mode
if (assert.strict) {
  assert = assert.strict;
} else {
  assert.deepEqual = assert.deepStrictEqual;
  assert.notDeepEqual = assert.notDeepStrictEqual;
  assert.equal = assert.strictEqual;
  assert.notEqual = assert.notStrictEqual;
}

const _ = require(".");

_.modifyPrototype();

function qdotFunc(value) {
  return _(value);
}

function qdotProto(value) {
  return value._;
}

function testPrimitiveGet(createProxy, original) {
  const proxy = createProxy(original);

  // support back to node 6.0.0
  // it seems NaN support for assert.equal was added in node 10.0.0
  if (Number.isNaN(original)) {
    assert.ok(Number.isNaN(proxy()));
  } else {
    assert.equal(proxy(), original);
  }

  assert.equal(proxy._(), undefined);
  assert.equal(proxy._._(), undefined);
  assert.equal(proxy._._._(), undefined);
  assert.equal(proxy.a(), undefined);
  assert.equal(proxy._.a(), undefined);
  assert.equal(proxy.a._(), undefined);
  assert.equal(proxy.a.b(), undefined);
  assert.equal(proxy.a.b.c(), undefined);
  assert.equal(proxy.a._.b._.c(), undefined);
  assert.equal(proxy.a["?."](), undefined);
  assert.equal(proxy[Symbol()](), undefined);
  assert.equal(proxy.a[Symbol()](), undefined);
  assert.equal(proxy.a[Symbol()].b(), undefined);

  if (original === null || Array.isArray(original)) {
    // typeof null and [] === 'object', but they should return {}
    assert.notEqual(proxy(typeof original), original);
    assert.deepEqual(proxy(typeof original), {});
  } else if (original !== undefined) {
    // support back to node 6.0.0
    // it seems NaN support for assert.equal was added in node 10.0.0
    if (Number.isNaN(original)) {
      assert.ok(Number.isNaN(proxy(typeof original)));
    } else {
      assert.equal(proxy(typeof original), original);
    }

    assert.equal(proxy[0](), original[0]);
    assert.equal(proxy[1](), original[1]);
  }
}

function testPopulatedGet(createProxy) {
  const symbol = Symbol();
  const populated = {
    array: [7, 8, 9],
    boolean: true,
    Boolean: new Boolean(true),
    function: function () {
      return "qdot";
    },
    null: null,
    number: 42,
    Number: new Number(42),
    object: { q: "dot" },
    string: "qdot",
    String: new String("qdot"),
    undefined: undefined,
    symbol: Symbol(),
    Symbol: new Object(Symbol()),
    [symbol]: Symbol(),

    nest: { a: { b: { c: null } } },
  };
  const proxy = createProxy(populated);

  assert.equal(proxy.array(), populated.array);
  assert.equal(proxy.array[0](), populated.array[0]);
  assert.equal(proxy.boolean(), populated.boolean);
  assert.equal(proxy.Boolean(), populated.Boolean);
  assert.equal(proxy.function(), populated.function);
  assert.equal(proxy.function()(), populated.function());
  assert.equal(proxy.null(), populated.null);
  assert.equal(proxy.number(), populated.number);
  assert.equal(proxy.Number(), populated.Number);
  assert.equal(proxy.object(), populated.object);
  assert.equal(proxy.object.q(), populated.object.q);
  assert.equal(proxy.string(), populated.string);
  assert.equal(proxy.String(), populated.String);
  assert.equal(proxy.undefined(), populated.undefined);
  assert.equal(proxy.symbol(), populated.symbol);
  assert.equal(proxy.Symbol(), populated.Symbol);
  assert.equal(proxy[symbol](), populated[symbol]);
  assert.equal(proxy.nest(), populated.nest);
  assert.equal(proxy.nest.a(), populated.nest.a);
  assert.equal(proxy.nest.a.b(), populated.nest.a.b);
  assert.equal(proxy.nest.a.b.c(), populated.nest.a.b.c);
  assert.equal(proxy.nest.a.b.c.d(), undefined);
}

function testSafeGet(createProxy) {
  const emptyProxy = createProxy({});

  // undefined values
  assert.deepEqual(emptyProxy.array("array"), []);
  assert.equal(emptyProxy.boolean("boolean"), false);
  assert.equal(
    emptyProxy.function("function").toString(),
    (() => {}).toString()
  );
  assert.equal(emptyProxy.number("number"), 0);
  assert.deepEqual(emptyProxy.object("object"), {});
  assert.equal(emptyProxy.string("string"), "");

  const populated = {
    array: [7, 8, 9],
    boolean: true,
    function: function () {
      return "qdot";
    },
    number: 42,
    object: { q: "dot" },
    string: "qdot",
  };
  const populatedProxy = createProxy(populated);

  // correct types
  assert.deepEqual(populatedProxy.array("array"), populated.array);
  assert.equal(populatedProxy.boolean("boolean"), populated.boolean);
  assert.equal(
    populatedProxy.function("function").toString(),
    populated.function.toString()
  );
  assert.equal(populatedProxy.number("number"), populated.number);
  assert.deepEqual(populatedProxy.object("object"), populated.object);
  assert.equal(populatedProxy.string("string"), populated.string);

  // incorrect types
  assert.deepEqual(populatedProxy.array("object"), {});
  assert.deepEqual(populatedProxy.boolean("array"), []);
  assert.deepEqual(populatedProxy.object("array"), []);
  assert.equal(populatedProxy.number("boolean"), false);
  assert.equal(
    populatedProxy.array("function").toString(),
    (() => {}).toString()
  );
  assert.equal(populatedProxy.string("number"), 0);
  assert.deepEqual(populatedProxy.number("object"), {});
  assert.equal(populatedProxy.function("string"), "");
}

function testEmptySet(createProxy) {
  const symbol = Symbol();
  const original = {};
  const proxy = createProxy(original);
  const abcd = proxy.a.b.c.d;

  assert.equal(abcd(), undefined);
  proxy.a.b.c.d = 65;
  assert.equal(abcd(), 65);

  proxy.a.b.x.y = 123;
  proxy.a[symbol].b = true;
  assert.deepEqual(original, {
    a: { b: { c: { d: 65 }, x: { y: 123 } }, [symbol]: { b: true } },
  });

  proxy.a = 7;
  assert.equal(abcd(), undefined);
  assert.deepEqual(original, { a: 7 });

  assert.throws(
    () => (createProxy(null).a = 7),
    TypeError,
    "should't be able to assign to null"
  );
  assert.throws(
    () => (createProxy(undefined).a = 7),
    TypeError,
    "should't be able to assign to undefined"
  );
}

function testPopulatedSet(createProxy) {
  const original = {
    array: [6, 7, 8],
    boolean: false,
    function: function () {
      return "qdot";
    },
    null: null,
    number: 1.618,
    object: { o: {} },
    string: "cue dot",
    symbol: Symbol("cue dot"),
    undefined: undefined,
  };
  const proxy = createProxy(original);

  assert.equal(proxy.array[0](), 6);
  proxy.array[0] = 2;
  assert.equal(proxy.array[0](), 2);
  proxy.array[7].const.g = 9.81;
  assert.deepEqual(proxy.array[7](), { const: { g: 9.81 } });

  assert.equal(proxy.boolean(), original.boolean);
  proxy.boolean.a = 6;
  proxy.boolean.b.c = 7;
  assert.equal(proxy.boolean(), original.boolean);

  assert.equal(proxy.function(), original.function);
  proxy.function.a = 6;
  proxy.function.b.c = 7;
  assert.equal(proxy.function(), original.function);

  assert.equal(proxy.null(), original.null);
  proxy.null.a = 6;
  proxy.null.b.c = 7;
  assert.equal(proxy.null(), original.null);

  assert.equal(proxy.number(), original.number);
  proxy.number.a = 6;
  proxy.number.b.c = 7;
  assert.equal(proxy.number(), original.number);

  assert.equal(proxy.object(), original.object);
  proxy.object.a = 6;
  proxy.object.b.c = 7;
  assert.deepEqual(proxy.object(), { a: 6, b: { c: 7 }, o: {} });

  assert.equal(proxy.string(), original.string);
  proxy.string.a = 6;
  proxy.string.b.c = 7;
  assert.equal(proxy.string(), original.string);

  assert.equal(proxy.symbol(), original.symbol);
  proxy.symbol.a = 6;
  proxy.symbol.b.c = 7;
  assert.equal(proxy.symbol(), original.symbol);

  assert.equal(proxy.undefined(), original.undefined);
  proxy.undefined.a = 6;
  proxy.undefined.b.c = 7;
  assert.deepEqual(proxy.undefined(), { a: 6, b: { c: 7 } });
}

//
// function tests
//
const testPrimitiveGetFunc = testPrimitiveGet.bind(null, qdotFunc);
testPrimitiveGetFunc(true);
testPrimitiveGetFunc(3.14);
testPrimitiveGetFunc(NaN);
testPrimitiveGetFunc("qdot");
testPrimitiveGetFunc(null);
testPrimitiveGetFunc(undefined);
testPrimitiveGetFunc(Object.create(null));
testPrimitiveGetFunc({});
testPrimitiveGetFunc([]);
testPrimitiveGetFunc(function () {});
testPrimitiveGetFunc(() => {});
testPrimitiveGetFunc(new Date());
testPrimitiveGetFunc(Symbol("qdot"));
testPrimitiveGetFunc(new Boolean(true));
testPrimitiveGetFunc(new Number(6.022e23));
testPrimitiveGetFunc(new String("qdot"));
testPrimitiveGetFunc(new Object(Symbol("qdot")));

testPopulatedGet(qdotFunc);
testSafeGet(qdotFunc);

testEmptySet(qdotFunc);
testPopulatedSet(qdotFunc);

//
// prototype tests
//
const testPrimitiveGetProto = testPrimitiveGet.bind(null, qdotProto);
testPrimitiveGetProto(true);
testPrimitiveGetProto(3.14);
testPrimitiveGetFunc(NaN);
testPrimitiveGetProto("qdot");
assert.throws(
  () => testPrimitiveGetProto(null),
  TypeError,
  "null shouldn't have a prototype"
);
assert.throws(
  () => testPrimitiveGetProto(undefined),
  TypeError,
  "undefined shouldn't have a prototype"
);
assert.throws(
  () => testPrimitiveGetProto(Object.create(null)),
  TypeError,
  "null prototype shouldn't inherit from Object"
);
testPrimitiveGetProto({});
testPrimitiveGetProto([]);
testPrimitiveGetProto(function () {});
testPrimitiveGetProto(() => {});
testPrimitiveGetProto(new Date());
testPrimitiveGetProto(Symbol("qdot"));

// todo:
// testPrimitiveGetFunc(new Boolean(true));
// testPrimitiveGetFunc(new Number(6.022e23));
// testPrimitiveGetFunc(new String("qdot"));
// testPrimitiveGetFunc(new Object(Symbol("qdot")));

testPopulatedGet(qdotProto);
testSafeGet(qdotProto);

testEmptySet(qdotProto);
testPopulatedSet(qdotProto);
