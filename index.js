const qdotProxies = new WeakSet();

function createProxy(rootValue, proxyPath, isProto) {
  const proxy = new Proxy(() => {}, {
    /**
     * Retrieve the value at proxyPath from rootValue tht this proxy represents.
     * If specified, always return a value of the given type.
     */
    apply(target, thisArg, argumentList) {
      const expectedType = argumentList[0];
      let value = rootValue;

      // traverse rootValue
      for (const segment of proxyPath) {
        if (value == null) {
          // short circuit the rest of the traversal
          value = undefined;
          break;
        }

        value = value[segment];
      }

      if (qdotProxies.has(value)) {
        // prevent recursive qdot in prototype mode
        value = undefined;
      } else if (
        isProto &&
        (value instanceof Boolean ||
          value instanceof Number ||
          value instanceof String ||
          value instanceof Symbol)
      ) {
        /* If qdot is on the Object prototype, primitive types will turn into
         * their Object counterparts. So turn them back into primitives here.
         *
         * caveat:
         *   The only time this breaks is if qdot is used directly with objects
         * from primitives like `new String("a")._()` or `new Number(5)._()`.
         * These will return `"a"` and `5` respectively, so strict comparing
         * with the original object will no longer work.
         *   Since primitives are already turned into Objects by the time
         * they get to qdot in prototype mode, there is no way to distinguish
         * them.
         *   This should hopefully be rare enough to not matter. Why are you
         * using String objects anyway?
         */
        value = value.valueOf();
      }

      if (expectedType === "array") {
        if (!Array.isArray(value)) {
          return [];
        }
      } else if (expectedType === "boolean") {
        if (typeof value !== "boolean") {
          return false;
        }
      } else if (expectedType === "function") {
        if (typeof value !== "function") {
          return () => {};
        }
      } else if (expectedType === "number") {
        if (typeof value !== "number") {
          return 0;
        }
      } else if (expectedType === "object") {
        if (
          typeof value !== "object" ||
          value === null ||
          Array.isArray(value)
        ) {
          return {};
        }
      } else if (expectedType === "string") {
        if (typeof value !== "string") {
          return "";
        }
      }

      return value;
    },

    /**
     * Get a virtual child value representing a path from the rootValue.
     */
    get(target, prop, receiver) {
      return createProxy(rootValue, proxyPath.concat(prop));
    },

    /**
     * Deep set a value at proxyPath from rootValue. Creates empty objects along
     * the path if one doesn't exist or is undefined. IF a value is deep set in
     * something other than an object then nothing happens and the value is
     * swalloed.
     */
    set(target, prop, newValue) {
      let child = rootValue;

      // traverse rootValue, creating new empty objects along the way if needed
      for (const segment of proxyPath) {
        if (child[segment] === undefined) {
          child[segment] = {};
        } else if (
          typeof child[segment] != "object" ||
          child[segment] === null
        ) {
          // short circuit traversal if trying to set value on primitive
          return;
        }

        child = child[segment];
      }

      child[prop] = newValue;
    },
  });

  qdotProxies.add(proxy);

  return proxy;
}

exports = module.exports = (value) => {
  return createProxy(value, [], false);
};

module.exports.modifyPrototype = (prop = "_") => {
  Object.defineProperty(Object.prototype, prop, {
    // configureable: true,
    get() {
      return createProxy(this, [], true);
    },
  });
};
